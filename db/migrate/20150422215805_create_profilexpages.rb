class CreateProfilexpages < ActiveRecord::Migration
  def change
    create_table :profilexpages do |t|
      t.belongs_to :page, index:true
      t.belongs_to :profile, index:true
      t.integer :page_id
      t.integer :profile_id
      t.integer :privilige

      t.timestamps null: false
    end
  end
end
