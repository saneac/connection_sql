class AddNameToUsers < ActiveRecord::Migration
  def change
    add_column :users, :profile_id, :integer
    add_column :users, :name,      :string
    add_column :users, :last_name, :string
    add_column :users, :birthday,  :date
    add_column :users, :boss_name, :string
    add_column :users, :phone,     :string
    add_column :users, :ext,       :string
  end
end
